<?php

namespace App\Transformers;

use App\Article;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [];

    protected $defaultIncludes = [];

    public function transform (Article $article)
    {
        return [
            'id' => $article->id,
            'author_id' => $article->author_id,
            'title' => $article->title,
            'created_at' => $article->created_at->format('d M Y'),
            'updated_at' => $article->updated_at->format('d M Y'),
        ];
    }
}