<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 * @package App
 *
 * @property int id
 * @property int author_id
 * @property string title
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Article extends Model
{
    protected $table = 'articles';
}
