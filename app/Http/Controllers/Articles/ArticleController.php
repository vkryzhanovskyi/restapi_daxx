<?php

namespace App\Http\Controllers\Articles;

use App\Http\Requests\Articles\StoreArticleRequest;
use App\Http\Requests\Articles\UpdateArticleRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ArticleTransformer;

class ArticleController extends Controller
{
    public function index() {
        $articles = Article::all();

        return Fractal::collection($articles, new ArticleTransformer);
    }

    public function store(StoreArticleRequest $request) {
        $article = new Article;
        $article->author_id = $request->user()->id;
        $article->title = $request->title;
        $article->save();

        return Fractal::item($article, new ArticleTransformer);
    }

    public function show(Article $article) {
        return Fractal::item($article, new ArticleTransformer);
    }

    public function update(UpdateArticleRequest $request, Article $article) {
        $article->title = $request->get('title', $article->title);
        $article->save();

        return Fractal::item($article, new ArticleTransformer);
    }

    public function destroy(Article $article) {
        $article->delete();

        return response(null, 200);
    }
}
